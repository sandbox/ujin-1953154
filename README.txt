=====
Ninesixty block helper
http://drupal.org/project/ninesixty_block_helper
-----
Ninesixty block helper was developed by Ujin 
<http://www.klynnwebsiteconsulting.com>.


=====
Installation
-----

1. Install as usual, see 
http://drupal.org/documentation/install/modules-themes/modules-7 
for more information.
2. To manage 960gs classes to a block, you should go to block's configuration 
page at Administration > Structure > Blocks
